#include <iostream>
#include <string>
#include <algorithm>
#include <cassert>
using namespace std;

class MyBigNumber
{
public:
    string sum(string num1, string num2)
    {
        string result = "";
        int carry = 0;
        int i = num1.length() - 1;
        int j = num2.length() - 1;

        while (i >= 0 || j >= 0)
        {
            int digit1 = (i >= 0) ? num1[i] - '0' : 0;
            int digit2 = (j >= 0) ? num2[j] - '0' : 0;
            int sum = digit1 + digit2 + carry;

            if (sum > 9)
            {
                carry = 1;
                sum %= 10;
            }
            else
            {
                carry = 0;
            }

            result += to_string(sum);

            i--;
            j--;
        }

        if (carry > 0)
        {
            result += to_string(carry);
        }

        reverse(result.begin(), result.end());

        return result;
    }

    void testSum()
    {
        assert(sum("0", "0") == "0");
        assert(sum("0", "100") == "100");
        assert(sum("123", "456") == "579");
        assert(sum("123456789", "987654321") == "1111111110");
        assert(sum("1234567890123456789", "9876543210123456789") == "11111111100246913578");
    }
};

int main()
{
    MyBigNumber myBigNumber;
    myBigNumber.testSum();
    cout << "All test cases passed\n";
    return 0;
}